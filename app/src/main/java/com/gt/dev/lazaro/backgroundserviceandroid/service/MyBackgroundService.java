package com.gt.dev.lazaro.backgroundserviceandroid.service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by lazarodev on 10/8/2017.
 */

public class MyBackgroundService extends Service {

    private Looper looper;
    private ServiceHandler serviceHandler;

    @Override
    public void onCreate() {
        // Creamos un manejador en segundo plano para correr nuestro servicio
        HandlerThread thread = new HandlerThread("HandlerTest", Process.THREAD_PRIORITY_BACKGROUND);

        // Inicia en el nuevo hilo del manejador
        thread.start();

        looper = thread.getLooper();

        // Empieza el servicio utilizando el manejador en segundo plano
        serviceHandler = new ServiceHandler(looper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        showToast("onStartCommand");
        // Llama al nuevo manejador de servicios. El ID del servicio puede ser utilizado para identificar el mismo
        Message message = serviceHandler.obtainMessage();
        message.arg1 = startId;
        serviceHandler.sendMessage(message);

        return START_STICKY;
    }

    protected void showToast(final String msg) {
        // obtenemos el hilo principal
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private final class ServiceHandler extends Handler {

        public ServiceHandler(Looper looper) {
            super(looper);
        }

        /**
         * En este metodo escribimos el codigo necesario para realizar
         * la tarea de este servicio. Basicamente sucede toda la magia
         * del servicio.
         *
         * @param msg
         */
        @Override
        public void handleMessage(Message msg) {
            try {
                Thread.sleep(5000);
                Log.i("MensajeManejador", "Tanto codigo para que termine en 5 segundos xD");
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
            showToast("Hola soy: " + msg.arg1 + " y voy a destruir tu telefono");
            stopSelf(msg.arg1);
        }
    }

}
