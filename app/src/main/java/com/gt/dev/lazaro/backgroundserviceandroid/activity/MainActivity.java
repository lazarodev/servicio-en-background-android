package com.gt.dev.lazaro.backgroundserviceandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.gt.dev.lazaro.backgroundserviceandroid.R;
import com.gt.dev.lazaro.backgroundserviceandroid.service.MyBackgroundService;

public class MainActivity extends AppCompatActivity {

    private Button btnStartService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnStartService = (Button) findViewById(R.id.btn_start_service);

        btnStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MyBackgroundService.class);
                startService(i);
            }
        });
    }
}
